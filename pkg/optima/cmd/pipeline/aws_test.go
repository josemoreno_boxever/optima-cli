package pipeline_test

import (
	"testing"

	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/pipeline"
)

func _TestPipelineLauncher(t *testing.T) {
	launcher := pipeline.NewPipeline("paddypower", "labs")
	err := launcher.Start()
	if err != nil {
		t.Fatal(err)
	}
}

func TestPipelineLauncherFQName(t *testing.T) {
	tests := []struct {
		Name, Env, Expected string
	}{
		{Name: "x", Env: "y", Expected: "x-y"},
		{Name: "customer", Env: "env", Expected: "customer-env"},
		{Name: "customer-uat", Env: "env", Expected: "customer-uat-env"},
	}

	for _, tt := range tests {
		lanuncher := pipeline.NewPipeline(tt.Name, tt.Env)
		fqName := lanuncher.FQName()
		if fqName != tt.Expected {
			t.Fatalf("expected: %s, Got: %s", tt.Expected, fqName)
		}
	}
}

func TestPipelineLauncherFuncName(t *testing.T) {
	tests := []struct {
		Name, Env, Expected string
	}{
		{Name: "x", Env: "y", Expected: "boxever-optima-x-y-cloudwatch-run-optima"},
		{Name: "customer", Env: "env", Expected: "boxever-optima-customer-env-cloudwatch-run-optima"},
		{Name: "customer-uat", Env: "env", Expected: "boxever-optima-customer-uat-env-cloudwatch-run-optima"},
	}

	for _, tt := range tests {
		lanuncher := pipeline.NewPipeline(tt.Name, tt.Env)
		funcName := lanuncher.FuncName()
		if funcName != tt.Expected {
			t.Fatalf("expected: %s, Got: %s", tt.Expected, funcName)
		}
	}
}
