package pipeline

import (
	"fmt"
)

const Name = "pipeline"

type Command struct {
	subcmd   string
	customer string
	env      string
}

type subcomamnd string

const (
	status  = "status"
	start   = "start"
	history = "history"
)

func NewCommand(args []string) (*Command, error) {
	// TODO parse args and create StatusCommand
	if len(args) < 3 {
		return nil, fmt.Errorf("No valid pipeline subcommand. Expected subcomand (status or start), customer and environment")
	}
	subcmd := args[0]
	switch subcmd {
	case status, start, history:
		return createCmd(args)
	default:
		return nil, fmt.Errorf("No valid pipeline subcommand. Expected 'status' or 'start', but got %s", subcmd)
	}
}

func (cmd *Command) Run() error {
	p := NewPipeline(cmd.customer, cmd.env)
	switch cmd.subcmd {
	case status:
		return p.Status()
	case start:
		return p.Start()
	case history:
		return p.History()
	}
	return nil
}

func createCmd(args []string) (*Command, error) {
	return &Command{
		subcmd:   args[0],
		customer: args[1],
		env:      args[2],
	}, nil

}
