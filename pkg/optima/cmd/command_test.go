package cmd_test

import (
	"os"
	"reflect"
	"testing"

	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/help"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/pipeline"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/status"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/usage"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/version"
)

func TestArgsParser(t *testing.T) {
	tests := []struct {
		args         []string
		expectedType reflect.Type
	}{
		{args: []string{"help"}, expectedType: reflect.TypeOf((*help.Command)(nil))},
		{args: []string{"status"}, expectedType: reflect.TypeOf((*status.Command)(nil))},
		{args: []string{"version"}, expectedType: reflect.TypeOf((*version.Command)(nil))},
		{args: []string{"version", "all"}, expectedType: reflect.TypeOf((*version.Command)(nil))},
		{args: []string{"usage"}, expectedType: reflect.TypeOf((*usage.Command)(nil))},
		{args: []string{"pipeline", "status", "customer", "env"}, expectedType: reflect.TypeOf((*pipeline.Command)(nil))},
		{args: []string{"pipeline", "start", "customer", "env"}, expectedType: reflect.TypeOf((*pipeline.Command)(nil))},
	}

	for _, test := range tests {
		os.Args = append([]string{"optima"}, test.args...)
		cmd, err := cmd.ArgsParser()
		if err != nil {
			t.Error("Error parsing args", err)
		}
		if reflect.TypeOf(cmd) != test.expectedType {
			t.Errorf("Unexpected type. Expected: %v, Got: %v", test.expectedType, reflect.TypeOf(cmd))
		}
	}
}

func BenchmarkArgsParser(b *testing.B) {
	os.Args = []string{"optima", "pipeline", "status", "customer", "env"}
	// run the ArgsParser function b.N times
	for n := 0; n < b.N; n++ {
		cmd.ArgsParser()
	}
}
