package help

import (
	"fmt"
)

const Name = "help"

type Command struct {
}

func NewCommand(args []string) (*Command, error) {
	return &Command{}, nil
}

func (cmd *Command) Run() error {
	fmt.Println(helpMessage)
	return nil
}

const helpMessage = `Optima is a tool for managing Optima pipeline, etc.

Usage:

    optima <command> <subcommand> [parameters]

The commands are:

    optima help
    optima pipeline [start|status|history] <customer> <environment>
    optima status
    optima usage
    optima version [all]

The command description:

    help       print the command help
    pipeline   to work with Optima pipelines
    status     show pipelines for current logged account
    version    print Optima version
`
