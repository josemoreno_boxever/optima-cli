package usage

import (
	"fmt"
)

const Name = "usage"

type Command struct {
}

func NewCommand(args []string) (*Command, error) {
	return &Command{}, nil
}

func (cmd *Command) Run() error {
	fmt.Println(usageMessage)
	return nil
}

const usageMessage = `Usage:

    optima <command> <subcommand> [parameters]

The commands are:

    optima help
    optima pipeline [start|status|history] <customer> <environment>
    optima status
    optima usage
    optima version [all]
`
