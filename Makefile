EXECUTABLES = go asciidoctor	# List of executables
CHECK_EXECUTABLES := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) command in PATH. Check README.md for build requirements")))

prefix=/usr/local
installdir=$(prefix)/opt/optima-cli
bindir=$(prefix)/bin
completiondir=$(prefix)/etc/bash_completion.d/
manpagesdir=$(prefix)/share/man/man1/

build=$(shell git rev-parse --short HEAD)
version=$(shell cat VERSION)
buildDate=$(shell date +%Y-%m-%dT%H:%M:%S%z)

.DEFAULT_GOAL := all

all: clean optima

clean:
	@ if [ -f optima ] ; then \
	  rm optima ; \
	fi;
	@ if [ -f optima.1 ] ; then \
	  rm optima.1 ; \
	fi;
	@ if [ -d dist ] ; then \
	  rm -rf dist ; \
	fi;


optima:
	@ go build  -ldflags "-w  -s -X 'main.buildVersion=$(version)' -X 'main.buildID=$(build)' -X 'main.buildDate=$(buildDate)'" \
	            -o optima cmd/optima-cli/main.go

check:
	@ go test -cover ./... 

# run benchmark test
check-bench:
	@ go test -bench=. ./...

man:
	@ asciidoctor -b manpage --destination-dir . docs/optima.adoc

install: install-optima install-man

install-optima:
	@ if [ -f optima ] ; then \
		sudo mkdir -p $(installdir)/bin $(installdir)/scripts ; \
		sudo chown -R $(shell whoami):admin $(installdir) ; \
        cp optima $(installdir)/bin/ ; \
	    cp scripts/optima-completion.bash $(installdir)/scripts/optima-completion.bash ; \
        ln -s -f $(installdir)/bin/optima $(bindir)/optima ; \
		ln -s -f $(installdir)/scripts/optima-completion.bash $(completiondir)/ ; \
	  else \
	  	echo "Program does not exist. Run 'make clean all' to build it first." ; \
	  fi;

install-man: man
	@ if [ -f optima.1 ] ; then \
	    cp optima.1 ${manpagesdir}/ ; \
	  fi;


uninstall: uninstall-optima uninstall-man

uninstall-optima:
	@ if [ -f $(completiondir)/optima-completion.bash ] ; then \
	  rm -f $(completiondir)/optima-completion.bash ; \
	fi;
	@ if [ -d $(installdir) ] ; then \
	  rm -rf $(installdir) ; \
	fi;

uninstall-man:
	@ if [ -f $(manpagesdir)/optima.1 ] ; then \
	  rm -f $(manpagesdir)/optima.1 ; \
	fi;


.PHONY: dist
dist:
	@ if [ -f optima ] ; then \
	  rm -rf dist/ ; \
	  mkdir -p dist/optima-cli/bin ; \
	  cp optima dist/optima-cli/bin/ ; \
	  cp -r scripts/ dist/optima-cli/scripts/  ; \
	  cd dist ; \
	  tar -czvf optima-cli$(version).darwin-amd64.tar.gz optima-cli ; \
	  cd .. ; \
	  rm -rf dist/optima-cli/ ; \
	else \
	  	echo "Program does not exist. Run 'make clean all' to build it first." ; \
	fi;
